package mk.ukim.finki.wp.organizeme.service;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * @author Riste Stojanov
 */
public interface CurrentTimeService {

    LocalTime getCurrentTime();

    LocalDate getCurrentDate();
}
