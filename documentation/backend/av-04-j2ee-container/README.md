# J2EE container basics

Here we will explain how to create the basic J2EE container managed components in a Spring Boot project: 
- Servlets 
- Filters 
- Listeners 

Only the specifics creating these components in the Spring Boot project will be covered here, while everything else 
is nicely described in the book **Head First: JSP and Servlets**. 

## Create Spring Boot project

First lets create a basic Spring Boot project. For this purpose we will use the 
[Spring Boot project generator](http://start.spring.io). For the purpose of this course, we are going to use the 
[Gradle](https://gradle.org/) build tool that will help us with the project dependency management, building and other 
tasks automation. Also, we are going to use the **Java** syntax with the Spring Boot version **2.1.0** (the latest stable
in the moment). 

For the project metadata section we are going to use the following configuration: 
- Group: `mk.finki.ukim.wp`
- Artifact: `organizeme`

We will depend on the following libraries: 
- Web (Full-stack web development with Tomcat and Spring MVC)
- JPA (Java Persistence API including spring-data-jpa, spring-orm and Hibernate)
- Security (Secure your application via spring-security)
- DevTools (Spring Boot Development Tools)
- H2 (H2 database (with embedded support)
- MySQL (MySQL JDBC driver)


Then we hit the **Generate Project** button and we obtain a skeleton Spring Boot project with the previous configuration. 
As a result we will get an *zip* archive containing the project. Next, we should unzip this archive, and open in in the 
IDE of our choice. I will use the Idea Intellij IDE, where I will open the project by selecting from the menu:
*File -> Open Project* and than choosing the location of the extracted archive. Next, a windows for *Gradle* settings 
should appear, where only the first checkbox (*Use auto-import*) should be additionally set, and everything else may be left at the default 
values. After hitting *Ok* the project should be opened. 

## Spring Boot project structure

The project contains the following files/folders: 
- build.gradle
    - The configuration of the project Gradle tasks and dependencies. 
- gradlew and gradlew.bat
    - Gradle wrapper scripts. Used if gradle is not provided otherwise.
- src/main/java
    - The place where the java code should be written. By default, we have the `mk.ukim.finki.wp.organizeme` package 
    created, and inside we have the `OrganizemeApplication.java` class. 
    - `OrganizemeApplication.java`
        - This class has a main method that is used to run the application. It is annotated with `@SpringBootApplication`
        which tells that this is a Spring Boot application and everything that is inside this package should be scanned 
        in order to initialize the **Applicaton Context**. It also tells that all auto-configurations that will be found 
        in the classpath (including the one from the depending libraries) will be invoked in order to enrich the Application 
        Context with their configurations. 
        - If we invoke `Debug` (The developers should never `run` their applications, they should `Debug` them.) for this
        class, we can notice in the logs that we get a connection to a database, preconfigured authentication and 
        preconfigured URL mappings.
- src/main/resources
    - The folder that contain the application resources, deployed in the root of the project classpath (ie. at *classpath/*). 
    - Contain the application configuration in `application.properties` file, which is initially empty, which denotes 
    that the default auto-configurations should be used.
- src/test
    - This folder contains the tests for our application  
    
    
## Enabling J2EE annotations scanning

In order to enable the J2EE annotations to be handler, we must add the annotation `@ServletComponentScan` to the 
`OrganizemeApplication` class. This way, the `SpringApplication.run` method will pass all classes inside the package 
`mk.ukim.finki.wp.organizeme` recursively, and will search for the annotations: 
- `@WebServlet`
- `@WebFilter`
- `@WebListener`

Since we have included the **Web** dependency (`spring-boot-starter-web` from build.gradle) in our project, we already
have preconfigured Spring MVC with embedded **Apache Tomcat** J2EE container. Therefore, the `@ServletComponentScan` will
find all classes that are annotated with one of the annotations in the package `javax.servlet.annotation` and will 
programmatically register them to the embedded Apache Tomcat. Therefore, the `web.xml` is obsolete and is defined 
using the previously mentioned annotations. 

## Creating servlets

Lets create our first servlet: 

```java
@WebServlet(urlPatterns = "/asdf", name = "hello-world-servlet") // here we describe for which urls this servlet will 
                                                                 // get invoked, and its internal name         
public class SampleServlet extends HttpServlet {    // Each servlet is a java class that extends GenericServlet, or the 
                                                    // HTTP specific HttpServlet

    @Override
    protected void doGet(HttpServletRequest req,
                         HttpServletResponse resp) throws ServletException, IOException {       //This servlet will 
                                                                                                // support only GET requests

        String name = (String) req.getSession().getAttribute("firstName");  
        String ipAddress = req.getRemoteHost();                                                 
        String clientAgent = req.getHeader("User-Agent");

        PrintWriter out = resp.getWriter();

        out.println("<html>");
        out.println("<body>");

        out.println("<h1>");
        out.println("Hello " + name);
        out.println("</h1>");

        out.println("<div>");
        out.println("Your IP address is: " + ipAddress);
        out.println("</div>");


        out.println("<div>");
        out.println("Your agent is: " + clientAgent);
        out.println("</div>");

        out.println("</body>");
        out.println("</html>");
    }
}

```

This servlet is a showcase for accessing attributes an parameters from different scopes, as well as getting headers and 
other request specifics. 

## Creating filters

The filters are java classes that are annotated with `@WebFilter` and implement the `javax.servlet.Filter` interface. 

Here we are going to implement a filter that will check if the user is authenticated (`AuthFilter`) and another filter 
that will log out the user for the path `/logout` (`LogoutFilter`). 

### Authentication 

```java

@WebFilter      // without parameters means that this filter is always invoked
public class AuthFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain)
      throws IOException, ServletException {

        HttpServletResponse httpResp = (HttpServletResponse) servletResponse;
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;


        String name = (String) httpRequest
          .getSession()
          .getAttribute("firstName");
        String path = httpRequest.getServletPath();

        if (!"/login".equals(path) &&                           // prevents infinite redirects
            (name == null || name.isEmpty())) {    
            httpResp.sendRedirect("/login");                // if the user is not authenticated, show the /login path
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }


    }

    @Override
    public void destroy() {

    }
}
```

The `/login` path is handled by the `LoginServlet`: 

```java
@WebServlet(urlPatterns = "/login", name = "login-servlet")
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req,
                         HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter out = resp.getWriter();

        out.println("<html>");
        out.println("<body>");

        out.println("<h1>");
        out.println("Login ");
        out.println("</h1>");

        out.println("<form action=\"/login\" method=\"POST\">");
        out.println("Name <input type=\"text\" name=\"firstName\"/> <br/>");
        out.println("Name <input type=\"submit\" value=\"Submit\"/> <br/>");
        out.println("</form>");


        out.println("</body>");
        out.println("</html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();

        String name = req.getParameter("firstName");

        session.setAttribute("firstName", name);

        resp.sendRedirect("/asdf");

    }
}
```

This servlet displays a Login page for the `GET` requests, and adds the `firstName` request parameter in the session 
for the `POST` requests, and then redirects to the `/asdf` path. 

### LogoutFilter

The Logout filter intercepts the `/logout` path and invalidates the session. Then it redirects to`/login` path. 

```java
@WebFilter(urlPatterns = "/logout")
public class LogoutFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        httpRequest.getSession().invalidate();

        HttpServletResponse res = (HttpServletResponse) servletResponse;
        res.sendRedirect("/login");
    }

    @Override
    public void destroy() {

    }
}
```

## Creating Listeners

The listeners are java classes that are annotated with `@WebListener` and implement one of the `javax.servlet` listeners: 
- ServletRequestListener
- ServletRequestAttributeListener
- ServletContextAttributeListener
- HttpSessionAttributeListener
- HttpSessionBindingListener
- ServletContextListener
- HttpSessionListener



