# Developing the first react js application (Task and Activity monitor)

Before reading this tutorial, one should read the documentation in the [React main 
concepts](https://reactjs.org/docs/hello-world.html) section (all 12 links).

## `App.js` container component (Using other components)

1. Replace the content of the `App.js`'s  `render` method with the following code:

```
render() {
 return (
      <div className="container">

        <div className="row">
          <div className="col-4">
            <TaskEditor />
          </div>

          <div className="col-8">
            <Task />
            <Task />
            <Task />
            <Task/>
          </div>
        </div>

      </div>
    );
}

```

The code that is returned by the `render` method is not standard **HTML** syntax, but instead it uses a **JSX** syntax 
that is defined by the React engineers. This syntax enables combining **HTML** elements (the tags with lower-case 
starting name) with react components (which are the tags that has upper-case starting name) and with expressions. [From 
React doc](https://reactjs.org/docs/jsx-in-depth.html#user-defined-components-must-be-capitalized)
 

An introduction to the JSX syntax and its constrains can be found on this link: 
https://reactjs.org/docs/introducing-jsx.html. Make sure that you will understand this syntax before you continue with 
this tutorial. 


## Add and link empty components

You can notice that the `<Task />` and `<TaskEditor />` components are invalid and the application crushes. Lets first 
create these components and that we will fix this problem. 

Create a functional component in the following file `src/components/Task/task.js`. This component should have the 
following code: 

```javascript
import React from 'react';

const task = (props) => {
  return (
    <div></div>
  );
};

export default taskEditor;
```

Also create the TaskEditor component in `src/components/TaskEditor/taskEditor.js` with the following content:
```javascript
import React from 'react';

const task = (props) => {
  return (
    <div></div>
  );
};

export default taskEditor;
```

Now we have the components, but the application still crushes. This is because `App.js` has no way to know what do we 
mean with the tags `<Task />` and `<TaskEditor />`. Therefore we need to add imports for the appropriate components that 
will be used to render these tags using the following lines at the top of the `App.js` component: 

```javascript
import Task from '../Task/task';
import TaskEditor from '../TaskEditor/taskEditor';
```

## Adding task and taskEditor templates with styling 


Next, lets replace the `task.js` functional component with the following content: 

```
const task = (props) => {
  let width200 = {width: '200px'};    //#1
  let width100 = {width: '100px'};
  return (
    <div className="row">
      <div className="float-left">
        The task title that can be longer
      </div>
      <div className="float-left">
        Project | Client
      </div>
      <div className="float-right"
           style={width200}>          //#2
        10:30 PM - 12:13 AM
      </div>
      <div className="float-right"
           style={width100}>
        2:32:45
      </div>
      <div className="float-right">
        <button className="btn-circle btn-success fa fa-play"
                title="Start working"></button>

        <button className="btn-circle btn-success fa fa-plus"
                title="Log time"></button>
      </div>
    </div>
  )
};
```

- **#1** is a JSON object that represent a CSS styling. It is used as a value of the `style` attribute in **#2**. This 
way we are able to provide a private styles for our components that are not visible outside of them. 

In order to enable the styling of the button, install the `font-awesome` iconic font using the following terminal command: 
`npm install --save font-awesome`

Then import the library in the `App.js` file using: 
`import 'font-awesome/css/font-awesome.css';`

Also, for the round buttons, we need to add the following class in `index.css`: 

```css
.btn-circle {
  width: 30px;
  height: 30px;
  padding: 6px 0px;
  margin: 3px;
  border-radius: 15px;
  text-align: center;
  font-size: 12px;
  line-height: 1.42857;
}
```




## Component properties and JSX rendering 

[First read this documentation](https://reactjs.org/docs/jsx-in-depth.html)

Until now, we showed that the components provide reusable templates. Now, lets see how we can add customization of the 
component appearance and behavior using properties. 

The `prop` argument that we pass to each **functional component** represents the attributes used to the corresponding 
component tag (element). All component attributes are packed together into a JSON object and passed to the invocation of 
the functional component through the first argument, which is recommended to be named `props`. Then the values of the 
attributes can be accessed using `props.attributeName` or `props['attributeName']`. 


The **classfull components** accept the properties through their constructor, and must invoke the **super(props)** in 
order to avoid compilation errors. Then the values of the attributes can be accessed using the private component field 
`props` by invoking `this.props.attributeName` or `this.props['attributeName']`. 

Now, lets add dynamic behavior to the `task.js` component. In order to do so, we will define 3 JSON objects at the 
beginning of the `render()` method in `App.js`: 

```javascript
let task1 = {
      title: 'Av 01',
      done: true,
      project: {
        id: 1,
        name: 'Web Programming',
        client: {
          id: 1,
          name: 'FINKI'
        }
      },
      totalTime: '2:23:12',
      activity: [{
        date: '25.10.2018',
        from: '13:12:05',
        to: '14:02:00'
      }, {
        from: '14:00:00',
        to: '15:30:00'
      }]
    };

    let task2 = {
      title: 'Av 02',
      done: false,
      project: {
        id: 1,
        name: 'Web Programming',
        client: {
          id: 1,
          name: 'FINKI'
        }
      },
      totalTime: '2:23:12',
      activity: [{
        from: '13:12:05',
        to: '14:02:00'
      }, {
        from: '14:10:00',
        to: null
      }]
    };

    let task3 = {
      title: 'Av 03',
      done: false,
      project: {
        id: 1,
        name: 'Web Programming',
        client: {
          id: 1,
          name: 'FINKI'
        }
      },
      totalTime: null,
      activity: []
    };
```

Next, lets pass these tasks for display by modifying the three `<Task />` tags without attributes with the following content: 

```
<Task task={task1} />
<Task task={task2} />
<Task task={task3} />
```

This way, the object can be accessed in the `task.js` using the `props.task` expression. 




### Task.js with dynamic content

Lets update the return value of the `render()` method in `task.js` with the following content:

```
  return (
    <div>
      <div className="row">
        <div className="float-left"
             style={margin}>
          {props.task.title}                                      //#1
        </div>
        <div className="float-left"
             style={margin}>
          <a href="">{props.task.project.name}</a> |              //#2
          <a href="">{props.task.project.client.name}</a>         //#3
        </div>

        <div className="float-right"
             style={width100}>
          {props.task.totalTime}                                  //#4
        </div>
        <div className="float-right">
          <button className="btn-circle btn-success fa fa-play"
                  title="Start working"></button>

          <button className="btn-circle btn-success fa fa-plus"
                  title="Log time"></button>
        </div>
      </div>
      <div className="row clearfix">
        {activity}                                                //#5
      </div>
    </div>
  );
```

The lines **#1, #2, #3** and **#4** are accessing the corresponding properties of the `task` attribute, which was passed 
to this component. Note that this is a functional component and we get the provided task through `props.task` invocation. 
In JSX all expressions must bi in curly braces (ex.: `{props.task.title}`). 

The line **#5** embeds a variable named `activity`. This variable should be defined before the return statement in the 
following way: 

```javascript
  let activity = props.task.activity
    .map((el, index) => {                                         //#6
      return (                                                    //#7
        <div className="col-12">                                  //#8               
          {el.date} ({el.from} - {el.to})
        </div>
      );
  });
```

In the previous code we use the ES6 `map` function to transform an array of JSON objects into array of JSX elements. The 
map function accepts a transformation function of the form `(el, index) => { return transformedEl; }`, where this 
function is invoked for each element `el` of the original array, and `index` is its position in the original array. 

Once we start the application, we notice that there is an exception that we do not have a key to the JSX returning array. 
This is because React expects a `key` attribute when we are rendering an array of JSX elements. Therefore, we should add 
a unique `key` attribute to the element in line **#8**. Since the `index` in the array is unique, we will add the following 
attribute here: `key={index}`. Now the exception disappears. 




## Props are read only 

Now lets add a behavior to the play button. Lets define a function that will be executed when this button is clicked. 
This function should log the clicked element (**#1**), then should add a static object at the end of the task's activity 
array (**#2**), and then should register a function that will be executed each second (**#3**) using the native 
`setInterval()` function. The registered function modifies the `totalTime` attribute from the `props.task` object. 

```javascript
  const startWorkHandler = (event) => {
    console.log(event.target);                        //#1
    props.task.activity.push({                        //#2
      date: '25.10.2018',
      from: '14:56:43',
      to: null
    });
    setInterval(() => {                               //#3
      console.log('in time out');
      props.task.totalTime += '.';                    //#4
      if (props.task.totalTime.length > 15) {
        props.task.totalTime = '2:23:15';
      }
    }, 1000);
  };
```

Then add the following attribute to the first `<button>` element: `onClick={startWorkHandler}`. 


Once we execute the code with this modification, we note that the click occurs, since the target element is shown in the 
console log, but nothing else happens. 

This is because the `props` object should not be changed from inside the component ie. it is **READ ONLY** from the 
component's perspective. 

In order to fix this issue, we will move the update of the `task` object outside of the `task.js` component, and 
here we will just invoke a handler method that will be accepted through a property: 


```javascript
  const startWorkHandler = (event) => {
    console.log('[task.js]', event.target);
    props.startWork();

  };
```


**Note**: Beside strings, we can also send objects, other types of primitives and functions as attributes in React, and 
we can use them as such inside the components. If we want to do so, the value should be embedded in curly braces 
`attributeName={nonStringValue}`. 



## Using properties in class-full components

Now lets just reorganize the variables `task1`, `task2` and `task3` into an array variable named `tasks`, and lets 
replace the attributes in the `<Task>` tags with the corresponding values: 

```
 <Task task={this.state.tasks[0]}
                  startWork={startWorkHandler}/>          //#1
<Task task={this.state.tasks[1]}/>
<Task task={this.state.tasks[2]}/>
```

We also moved the `startWorkerHandler` function inside the `App.js`'s `render()` method, and we provide it to the `Task` 
component via the `startWork` attribute. 

**But again NOTHING HAPENS**. This is because the `tasks` variable is in the `render` method, and it is reinitialized 
each time the render method is invoked. However, the tasks are owned by the `App.js` component, and they describe its 
state. Therefore, they should be set in the components state field. 

Therefore, we define the tasks in the constructor of the `App.js` component with the following code: 

```javascript
  constructor(props) {
    super(props);                 //#1

    let tasks = [...]; 

     this.state = {               //#2
      tasks: tasks
    };
  }
```


The line **#1** is required in order to compile the component and in order to access the attributes via the `this.props` 
object.  The line **#2** is the state initialization. 


### Properties in class-full components

Lets add a title property to the `App` component by setting the following in `index.js`:

`ReactDOM.render(<App title="Organize Me" />, document.getElementById('root'));`

This property can be used in the `App.js` component with the expression: `{this.props.title}`.

### State access and updates 

Here is the modified version of the `startWorkerHandler` method moved into the `App.js` component, with the modifications
that update the `state`.  

```javascript
    const startWorkHandler = (index) => {
      let task1 = this.state.tasks[index];                      //#1
      console.log('[App.js] startWorkingHandler')
      task1.activity.push({                                     //#2
        date: '25.10.2018',
        from: '14:56:43',
        to: null
      });
      setInterval(() => {                                   
        console.log('in time out');
        task1.totalTime += '.';                                 //#3
        if (task1.totalTime.length > 15) {
          task1.totalTime = '2:23:15';
        }
        this.setState({tasks: this.state.tasks});               //#4
      }, 1000);

      this.setState({tasks: this.state.tasks});                 //#5
    };
```

In the line **#1** we obtain the task with the given `index` from the `tasks` array in the state. As you can notice, 
the state attributes are accessed from the `this.state` JSON object. In the lines **#2** and **#3** we are changing 
the object inside the state. In order to notify react that its state is modified, we invoke the `this.setState` method, 
that actually does not do anything, but replacing the tasks with the exact same existing reference in the state. 

Even though this is an anti-pattern, this code does the job in this scenario.  

## Handling state with immutable objects



The previous code has the following problems:
 - We modify an existing object from the state. In some cases, when child components are responsible for display of the
 corresponding objects, the components may not be aware about the modification since the object reference is not changed. 
 - We depend on the previous values of state properties in lines **#2** and **#3**. In these cases, React does not guarantee
 that `this.state` will contain the latest version of the state, since the state is asynchronously updated. Therefore, 
 it is recommended to use the form of `this.setState` that accepts a function as attribute.
 
 > Never mutate `this.state` directly, as calling setState() afterwards may replace the mutation you made. Treat 
 `this.state` as if it were immutable. [React State](https://reactjs.org/docs/react-component.html#state) 
 
Here is the more complex version of this method, where all of the modifications are on a new, cloned, objects and arrays: 

```javascript
    const cloneTasks = (state, index) => {                                      //#1
      const task = {
        ...state.tasks[index],                                                  //#2
        activity: [...state.tasks[index].activity]                              //#3
      };

      const newTasksArrayRef = [...state.tasks];                                //#4
      newTasksArrayRef[index] = task;                                           //#5
      return newTasksArrayRef;
    };

    const startWorkHandler = (index) => {
      console.log('[App.js] startWorkingHandler for index: ', index);

      this.setState((state, props) => {                                         //#6
        const newTasksArrayRef = cloneTasks(state, index);                      //#7
        const task = newTasksArrayRef[index];                                   //#8

        task.activity.push({
          date: '25.10.2018',
          from: '14:56:43',
          to: null
        });

        if (task.activeTimer) {
          console.log('There is timer that is already started!');               //#9
        } else {
          task.activeTimer = setInterval(() => {
            this.setState((state, props) => {                                   //#10
              const tasksInInterval = cloneTasks(state, index);                 
              const taskInInterval = tasksInInterval[index];
              console.log('in time out');
              taskInInterval.totalTime += '.';
              if (taskInInterval.totalTime.length > 15) {
                taskInInterval.totalTime = '2:23:15';
              }
              return {tasks: tasksInInterval};                                  //#11
            });

          }, 1000);
        }

        return {tasks: newTasksArrayRef};                                       //#12
      });

``` 

The previous code defines auxiliary function `cloneTasks` that replaces the reference of the tasks array, with a new one 
that contains the exact same tasks as the original one (line **#4**), and then replaces the task with the given `index`
with a shallow copy of its attributes. In the lines **#2, #3** and **#4** we use the [ES6 spread 
operator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax).

The state is modified with the `this.setState` variant of the method that accepts a function as argument. More about 
state updating can be read in the [React docs](https://reactjs.org/docs/state-and-lifecycle.html#using-state-correctly).
In the line **#6** we are setting the state with a function argument. This way we ensure the state consistency, since it  
is asynchronously updated, and we obtain the *up-to-date* version of the state as first argument. In line **#7** 
we obtain a sallow clone of the tasks array (**#4**), where the new `task` is added at the `index` position (**#5**).
We use constants (**#7,#8**) so that we won't accidentally change the references for these objects, and to explicitly 
state that `newTasksArrayRef` and `task` are immutable. 

The `task.activeTimer` describes if there is a timer that is already started, and when this is the case, we just log 
a console message (**#9**). If there is no active timer, we use the javascript native `setInterval(fn, repeatPeriodMs)` 
function to invoke a function `fn` every `repeatPeriodMs` milliseconds, which in our case is once per second.

In the function called every second we are using `this.setState((state,props)=>{...})` to update the state. As one can 
notice, we have a nested `this.setState` call in this line, but this is fine since this call is invoked every second, and 
therefore we have to take the *up-to-date* state using the function argument variant of `this.setState`.      

The argument function of `this.setState` returns an JSON object (**#11, #12**). This way we are telling RactJS to update
only the `tasks` field of our state with the new version of the array. The cloning is to ensure that ReactJS will see 
the change through the new reference and will update the nested components accordingly.   

  