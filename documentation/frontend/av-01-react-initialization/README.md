# Getting started with React

## Before we begin

React is a Javascript framework. If you are new in Javascript or you need some refreshment here is a brief introductory tutorial: https://developer.mozilla.org/en-US/docs/Web/JavaScript/A_re-introduction_to_JavaScript

Also you need to install (Node.js)[https://nodejs.org/en/]  in order to be able to create, build and run your application. 


##  Create the application 
Creating your first react application (https://github.com/facebook/create-react-app#create-react-app-: 

`npx create-react-app organizeme-react`

This command outputs: 

```
Creating a new React app in ~/wp2018/organizeme-react.

Installing packages. This might take a couple of minutes.
Installing react, react-dom, and react-scripts...

+ react-dom@16.5.2
+ react@16.5.2
+ react-scripts@2.0.5
added 1727 packages in 96.416s

Initialized a git repository.

Success! Created organizeme-react at ~/wp2018/organizeme-react
Inside that directory, you can run several commands:

  npm start
    Starts the development server.

  npm run build
    Bundles the app into static files for production.

  npm test
    Starts the test runner.

  npm run eject
    Removes this tool and copies build dependencies, configuration files
    and scripts into the app directory. If you do this, you can’t go back!

We suggest that you begin by typing:

  cd organizeme-react
  npm start

Happy hacking!
```

## Start the application

The application will start by executing: 
`npm start`

This command will automatically detect all code changes and will refresh the content in your browser. 


## Adding bootstrap

`npm install bootstrap --save`

The bootstrap css library depends on jQuery and pooper.js, and therefore they must be added with: 

`npm install jquery popper.js --save`

### Importing bootstrap

In the `index.js` file add the following line at the beginning:

`import 'bootstrap/dist/css/bootstrap.min.css';`


## Organize the folders

1. Create folder with name `components` in the root of the project. This folder will contain all the components. 
2. Create folder `components/App` that will contain the code for the `<App />` component. 
3. Move the files `App.css`, `App.js` and `App.test.js` into the folder `components/App`
4. Fix the `import App` error in the `index.js` file by replacing the line with: 
```import App from './components/App/App';```


